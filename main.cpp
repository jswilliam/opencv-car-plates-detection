/**
*  DMET 901 - Computer Vision
*  main.cpp
*  Assignment 2
*  Egyptian Licence Plates Regulation System
*
*  @author Joseph S. William 28-3921 T13
*/

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <iostream>
#include <vector>
#include <stdio.h>
#include <sstream>
#include <string>
#include <set>

using namespace cv;
using namespace std;

//Classes Definitions
class PixelClass {
public:
  int iLoc;
  int jLoc;
  int value1;
  int value2;
  int value3;
  int value4;
  PixelClass(int v1, int v2, int v3, int v4);
  PixelClass();
  int getValue(int index);
};

/**
* Contructor for PixelClass.
*
* @param the four class values of the four neighboring pixels
* @return void
*/
PixelClass::PixelClass(int v1, int v2, int v3, int v4)
{
  value1 = v1;
  value2 = v2;
  value3 = v3;
  value4 = v4;
}

/**
* Default empty contructor for PixelClass.
*
* @param void
* @return void
*/
PixelClass::PixelClass()
{
  iLoc = 0;
  jLoc = 0;
  value1 = 0;
  value2 = 0;
  value3 = 0;
  value4 = 0;
}

/**
* Gets the value of the class based in index.
*
* @param index of the value.
* @return the class value.
*/
int PixelClass::getValue(int index)
{
  if(index == 0)
  return value1;
  if(index == 1)
  return value2;
  if(index == 2)
  return value3;
  if(index == 3)
  return value4;
}

//to_string Patch
namespace patch
{
  template < typename T > std::string to_string( const T& n )
  {
    std::ostringstream stm ;
    stm << n ;
    return stm.str() ;
  }
}

//Methods Definitions
Mat loadImage( string imageName, int imageMode );
void showImage( string windowName, Mat image );
int getImageClassesCountBinary( string imageName );
int getImageClassesCountGrayscale( string imageName );
void printClasses(vector<PixelClass> pixelClasses);
bool hasOneClass(PixelClass p);
bool withinSameRange(uchar value1, uchar value2);
int replaceEqualClasses(vector<PixelClass> p);
void extractPlateInfo(string plateImage);
string bestMatchLetter();
string bestMatchNumber();
void initializeLibrary();
double getSimilarity( string imA, string imB ) ;
int largestClass(int a, int b, int c, int d);

//Global Variables
int COLOR_IMAGE = CV_LOAD_IMAGE_COLOR;
int GRAYSCALE_IMAGE = CV_LOAD_IMAGE_GRAYSCALE;
Mat classes(699,325,CV_32SC1);
string alphabet[38];
string extract[6];

/**
* Main Method
*
* @param unused arguments.
* @return 0.
*/
int main(int argc, char const *argv[])
{
  initializeLibrary();
  extractPlateInfo("L1.jpg");
  std::cout << "The number of classes in Binary L1 after Second Iteration (Replacing Equal Classes):  " << getImageClassesCountBinary("L1.jpg") << std::endl;
  std::cout << "The number of classes in Grayscale L3 frontal view after Second Iteration (Replacing Equal Classes):  " << getImageClassesCountGrayscale("L3.jpg") << std::endl;
  std::cout << '\n' << std::endl;
  std::cout << "/*** Extracted images from plate directory -> /extract ***/" << std::endl;
  std::cout << "/*** Library images directory -> /lib ***/" << std::endl;
  std::cout << "The plate Letters are:  " << bestMatchLetter() << std::endl;
  std::cout << "The plate Numbers are:  " << bestMatchNumber() << std::endl;
  waitKey(0);
  return 0;
}

/**
* Loads image from name
*
* @param the name of the image file.
* @return loaded image.
*/
Mat loadImage( string imageName, int imageMode )
{
  Mat image;
  image = imread( imageName, imageMode );

  if(! image.data )
  {
    cout <<  "Could not open or find the image" << std::endl ;
    return image;
  }

  return image;
}

/**
* Displays image from image matrix.
*
* @param name of the display window and the image matrix.
*/
void showImage( string windowName, Mat image )
{
  namedWindow( windowName , WINDOW_AUTOSIZE );
  imshow( windowName , image );
}

/**
* Loads image from name and converts it to binary then performs
* 8-connectivity to find the pixels' classes.
*
* @param the name of the image file.
* @return the number of classes after second iteration (replace equal classes).
*/
int getImageClassesCountBinary( string imageName )
{
  Mat imageOriginal = loadImage( imageName, GRAYSCALE_IMAGE);
  Mat image = imageOriginal;
  //classes = Mat::zeros(image.rows, image.cols, image.type());
  vector<PixelClass> pixelClasses;

  int classCount = 0;
  int isSimilar = 0;

  threshold(imageOriginal, image, 128.0, 255.0, THRESH_BINARY);

  for (int i = 0; i < image.rows; i++)
  {
    for (int j = 0; j < image.cols; j++)
    {
      int iUp = i - 1;
      int jLeft = j - 1;
      int jRight = j + 1;
      PixelClass pClass = PixelClass();
      pClass.iLoc = i;
      pClass.jLoc = j;

      if (iUp > -1)
      {
        if (image.at<uchar>(iUp,j) == image.at<uchar>(i,j))
        {
          isSimilar++;
          pClass.value1 = classes.at<int>(iUp,j);
          classes.at<int>(i,j) = classes.at<int>(iUp,j);
        }
      }
      if (jLeft > -1)
      {
        if (image.at<uchar>(i,jLeft) == image.at<uchar>(i,j))
        {
          isSimilar++;
          if(classes.at<int>(i,jLeft) != pClass.value1 && pClass.value1 != 0)
          pClass.value2 = classes.at<int>(i,jLeft);
          classes.at<int>(i,j) = classes.at<int>(i,jLeft);
        }
      }
      if (jRight < image.cols && iUp > -1)
      {
        if (image.at<uchar>(iUp,jRight) == image.at<uchar>(i,j))
        {
          isSimilar++;
          if(classes.at<int>(iUp,jRight) != pClass.value1 && classes.at<int>(iUp,jRight) != pClass.value2  && pClass.value1 != 0 && pClass.value2 != 0)
          pClass.value3 = classes.at<int>(iUp,jRight);
          classes.at<int>(i,j) = classes.at<int>(iUp,jRight);
        }
      }
      if (jLeft > -1 && iUp > -1)
      {
        if (image.at<uchar>(iUp,jLeft) == image.at<uchar>(i,j))
        {
          isSimilar++;
          if(classes.at<int>(iUp,jLeft) != pClass.value3 && classes.at<int>(iUp,jLeft) != pClass.value2 && classes.at<int>(iUp,jLeft) != pClass.value2 && pClass.value1 != 0 && pClass.value2 != 0 && pClass.value3 != 0)
          pClass.value4 = classes.at<int>(iUp,jLeft);
          classes.at<int>(i,j) = classes.at<int>(iUp,jLeft);
        }
      }
      if(isSimilar == 0)
      {
        classCount++;
        classes.at<int>(i,j) = classCount;
      }
      if(isSimilar > 1 && (pClass.value1 != 0 || pClass.value2 != 0 || pClass.value3 != 0 || pClass.value4 != 0) && !hasOneClass(pClass))
      {
        pixelClasses.push_back(pClass);
      }
      isSimilar = 0;
    }
  }
  //printClasses(pixelClasses);
  std::cout << "The number of classes in Binary L1 after First Iteration:  " << classCount << std::endl;
  return replaceEqualClasses(pixelClasses);
}

/**
* Loads image from name and converts it to grayscale then perfroms
* 8-connectivity to find the pixels' classes.
*
* @param the name of the image file.
* @return the number of classes after second iteration (replace equal classes).
*/
int getImageClassesCountGrayscale( string imageName )
{
  Mat image = loadImage( imageName, GRAYSCALE_IMAGE);
  //classes = Mat::zeros(image.rows, image.cols, image.type());
  vector<PixelClass> pixelClasses;
  int classCount = 0;
  int isSimilar = 0;

  for (int i = 0; i < image.rows; i++)
  {
    for (int j = 0; j < image.cols; j++)
    {
      int iUp = i - 1;
      int jLeft = j - 1;
      int jRight = j + 1;
      PixelClass pClass = PixelClass();
      pClass.iLoc = i;
      pClass.jLoc = j;

      if (iUp > -1)
      {
        if (withinSameRange(image.at<uchar>(iUp,j),image.at<uchar>(i,j)))
        {
          isSimilar++;
          pClass.value1 = classes.at<int>(iUp,j);
          classes.at<int>(i,j) = classes.at<int>(iUp,j);
        }
      }
      if (jLeft > -1)
      {
        if (withinSameRange(image.at<uchar>(i,jLeft),image.at<uchar>(i,j)))
        {
          isSimilar++;
          if(classes.at<int>(i,jLeft) != pClass.value1 && pClass.value1 != 0)
          pClass.value2 = classes.at<int>(i,jLeft);
          classes.at<int>(i,j) = classes.at<int>(i,jLeft);
        }
      }
      if (jRight < image.cols && iUp > -1)
      {
        if (withinSameRange(image.at<uchar>(iUp,jRight),image.at<uchar>(i,j)))
        {
          isSimilar++;
          if(classes.at<int>(iUp,jRight) != pClass.value1 && classes.at<int>(iUp,jRight) != pClass.value2  && pClass.value1 != 0 && pClass.value2 != 0)
          pClass.value3 = classes.at<int>(iUp,jRight);
          classes.at<int>(i,j) = classes.at<int>(iUp,jRight);
        }
      }
      if (jLeft > -1 && iUp > -1)
      {
        if (withinSameRange(image.at<uchar>(iUp,jLeft),image.at<uchar>(i,j)))
        {
          isSimilar++;
          if(classes.at<int>(iUp,jLeft) != pClass.value3 && classes.at<int>(iUp,jLeft) != pClass.value2 && classes.at<int>(iUp,jLeft) != pClass.value2 && pClass.value1 != 0 && pClass.value2 != 0 && pClass.value3 != 0)
          pClass.value4 = classes.at<int>(iUp,jLeft);
          classes.at<int>(i,j) = classes.at<int>(iUp,jLeft);
        }
      }
      if(isSimilar == 0)
      {
        classCount++;
        classes.at<int>(i,j) = classCount;
      }
      if(isSimilar > 1 && (pClass.value1 != 0 || pClass.value2 != 0 || pClass.value3 != 0 || pClass.value4 != 0) && !hasOneClass(pClass))
      {
        pixelClasses.push_back(pClass);
      }
      isSimilar = 0;
    }
  }
  std::cout << "The number of classes in Grayscale L3 frontal view after First Iteration:  " << classCount << std::endl;
  return replaceEqualClasses(pixelClasses);
}

/**
* Test method to print the equal classes of each pixel.
*
* @param a vector of PixelClass.
* @return void
*/
void printClasses(vector<PixelClass> pixelClasses)
{
  for(int i = 0; i < pixelClasses.size(); i++)
  std::cout << "(" << pixelClasses[i].iLoc << "," << pixelClasses[i].jLoc << ") - " << pixelClasses[i].value1 << ',' << pixelClasses[i].value2 << ',' << pixelClasses[i].value3 << ',' << pixelClasses[i].value4 << '\n';
}

/**
* Checks if the pixel does not have more than one class that are
* equal to each other.
*
* @param Object of type PixelClass.
* @return true if has only one class, false otherwise.
*/
bool hasOneClass(PixelClass p)
{
  int countZero = 0;
  if(p.value1 == 0)
  countZero++;
  if(p.value2 == 0)
  countZero++;
  if(p.value3 == 0)
  countZero++;
  if(p.value4 == 0)
  countZero++;
  if(countZero == 3)
  return true;

  return false;
}

/**
* Checks if the values are within the same class range for grayscale image.
*
* @param two value that are to be compared.
* @return true if in the same range, false otherwise.
*/
bool withinSameRange(uchar value1, uchar value2)
{
  int v1 = (int) value1;
  int v2 = (int) value2;
  if(v1 >= 0 && v1 <= 63 && v2 >= 0 && v2 <= 63)
  {
    return true;
  }
  if(v1 >= 64 && v1 <= 127 && v2 >= 64 && v2 <= 127)
  {
    return true;
  }
  if(v1 >= 128 && v1 <= 191 && v2 >= 128 && v2 <= 191)
  {
    return true;
  }
  if(v1 >= 192 && v1 <= 255 && v2 >= 192 && v2 <= 255)
  {
    return true;
  }
  return false;
}

/**
* Performs the second iteration on pixels' that have more than one class equal
* to each other
*
* @param a vector of PixelClass.
* @return the number of classes after second iteration.
*/
int replaceEqualClasses(vector<PixelClass> p)
{
  set<int> distinctClasses;
  for (int i = 0; i < p.size(); i++)
  {
    classes.at<int>(p[i].iLoc,p[i].jLoc) = largestClass(p[i].value1,p[i].value2,p[i].value3,p[i].value4);
  }
  for (int i = 0; i < classes.rows; i++)
  {
    for (int j = 0; j < classes.cols; j++)
    {
      distinctClasses.insert((int)classes.at<int>(i,j));
    }
  }
  return distinctClasses.size();
}

/**
* Extracts the plate numbers and letters and store them in /extract .
*
* @param the name of the plate image.
* @return void
*/
void extractPlateInfo(string plateImage)
{
  Mat image = loadImage(plateImage, COLOR_IMAGE);
  Rect myROI(20, 150, (image.cols) - 30, (image.rows) - 190);
  Mat croppedImage = image(myROI);
  imwrite("extract/cropped.png",croppedImage);

  Rect letters(35, 0, (croppedImage.cols)/2 - 100, croppedImage.rows);
  Rect numbers((croppedImage.cols)/2, 0, croppedImage.cols/2, croppedImage.rows);
  Mat cropeedLetters = croppedImage(letters);
  Mat cropeedNumbers = croppedImage(numbers);

  int index = 6;
  for (int i = 0; i < cropeedLetters.cols -1; i+=(cropeedLetters.cols/3))
  {
    Rect myROI2(i, 0, (cropeedLetters.cols)/3, cropeedLetters.rows);
    Mat croppedImage2 = cropeedLetters(myROI2);
    imwrite("extract/" + patch::to_string(index) + ".png",croppedImage2);
    extract[index - 1] = "extract/" + patch::to_string(index) + ".png";
    index--;
  }
  for (int i = 0; i < cropeedNumbers.cols -1; i+=(cropeedNumbers.cols/3))
  {
    Rect myROI2(i, 0, (cropeedNumbers.cols)/3, cropeedNumbers.rows);
    Mat croppedImage2 = cropeedNumbers(myROI2);
    imwrite("extract/" + patch::to_string(index) + ".png",croppedImage2);
    extract[index - 1] = "extract/" + patch::to_string(index) + ".png";
    index--;
  }
}

/**
* Iterates through the library to find the best match letter with refrence to
* the extracted letters from the plate image.
*
* @param void
* @return A string with the best match plate numbers
*/
string bestMatchLetter()
{
  double matchValue[38];
  string result = "";
  for (int i = 0; i < 3; i++) {
    for (int j = 10; j < 38; j++) {
      matchValue[j] = getSimilarity(extract[i],alphabet[j]);
    }
    double minSf = 1000;
    int bestMatchIndex = 0;
    for(int k = 10; k < 38 ;k++)
    {
      if(matchValue[k] < minSf)
      {
        minSf = matchValue[k];
        bestMatchIndex = k;
      }
    }
    showImage(alphabet[bestMatchIndex].substr(4,2),loadImage(alphabet[bestMatchIndex],GRAYSCALE_IMAGE));
    result = alphabet[bestMatchIndex].substr(4,2) + " " + result ;
  }
  return result;
}

/**
* Iterates through the library to find the best match number with refrence to
* the extracted numbers from the plate image.
*
* @param void
* @return A string with the best match plate numbers
*/
string bestMatchNumber()
{
  double matchValue[38];
  string result = "";
  for (int i = 3; i < 6; i++) {
    for (int j = 1; j < 10; j++) {
      matchValue[j] = getSimilarity(extract[i],alphabet[j]);
    }
    double minSf = 1000;
    int bestMatchIndex = 0;
    for(int k = 1; k < 10 ;k++)
    {
      if(matchValue[k] < minSf)
      {
        minSf = matchValue[k];
        bestMatchIndex = k;
      }
    }
    showImage(alphabet[bestMatchIndex].substr(4,2),loadImage(alphabet[bestMatchIndex],GRAYSCALE_IMAGE));
    result = alphabet[bestMatchIndex].substr(4,1) + " " + result ;
  }
  return result;
}

/**
* Initialize the arabic alphabet and numbers library.
*
* @param void
* @return void
*/
void initializeLibrary()
{
  alphabet[0] = "lib/0.jpg";
  alphabet[1] = "lib/1.jpg";
  alphabet[2] = "lib/2.jpg";
  alphabet[3] = "lib/3.jpg";
  alphabet[4] = "lib/4.jpg";
  alphabet[5] = "lib/5.jpg";
  alphabet[6] = "lib/6.jpg";
  alphabet[7] = "lib/7.jpg";
  alphabet[8] = "lib/8.jpg";
  alphabet[9] = "lib/9.jpg";
  alphabet[10] = "lib/ا.jpg";
  alphabet[11] = "lib/ب.jpg";
  alphabet[12] = "lib/ت.jpg";
  alphabet[13] = "lib/ث.jpg";
  alphabet[14] = "lib/ج.jpg";
  alphabet[15] = "lib/ح.jpg";
  alphabet[16] = "lib/خ.jpg";
  alphabet[17] = "lib/د.jpg";
  alphabet[18] = "lib/ذ.jpg";
  alphabet[19] = "lib/ر.jpg";
  alphabet[20] = "lib/ز.jpg";
  alphabet[22] = "lib/س.jpg";
  alphabet[21] = "lib/ش.jpg";
  alphabet[23] = "lib/ص.jpg";
  alphabet[24] = "lib/ض.jpg";
  alphabet[25] = "lib/ط.jpg";
  alphabet[26] = "lib/ظ.jpg";
  alphabet[27] = "lib/ع.jpg";
  alphabet[28] = "lib/غ.jpg";
  alphabet[29] = "lib/ف.jpg";
  alphabet[30] = "lib/ق.jpg";
  alphabet[31] = "lib/ك.jpg";
  alphabet[32] = "lib/ل.jpg";
  alphabet[33] = "lib/م.jpg";
  alphabet[34] = "lib/ن.jpg";
  alphabet[35] = "lib/ه.jpg";
  alphabet[36] = "lib/و.jpg";
  alphabet[37] = "lib/ى.jpg";
}

/**
* Compare two images by getting the square-root of sum of squared error.
*
* @param the name of the two images to be compared.
* @return the value of the square-root of sum of squared error.
*/
double getSimilarity( string imA, string imB )
{
  Mat A  = loadImage(imA, COLOR_IMAGE);
  Mat B = loadImage(imB, COLOR_IMAGE);
  threshold(A, A, 128.0, 255.0, THRESH_BINARY);
  threshold(B, B, 128.0, 255.0, THRESH_BINARY);
  resize(B, B, A.size(), 0, 0, INTER_NEAREST);
  if ( A.rows > 0 && A.rows == B.rows && A.cols > 0 && A.cols == B.cols ) {
    double errorL2 = norm( A, B, CV_L2 );
    double similarity = errorL2 / (double)( A.rows * A.cols );
    return similarity;
  }
  return -1;
}

/**
* Gets the largest number out of four.
*
* @param four numbers.
* @return largest number.
*/
int largestClass(int a, int b, int c, int d)
{
  int max = a, min = a;

  if(b > max)
  {
    max = b;
  }
  else if(b < min)
  {
    min = b;
  }
  if(c > max)
  {
    max = c;
  }
  else if(c < min)
  {
    min = c;
  }
  if(d > max)
  {
    max = d;
  }
  else if(d < min)
  {
    min = d;
  }
  return max;
}
